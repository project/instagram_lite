INSTAGRAM LITE
--------------

INTRODUCTION
------------

The already existing Instagram feed plugins that were worked without API got
broken since the latest Instagram changes. To address this issue you can use
this module to display your Instagram feed using the new Instagram Graph API.

INSTALLATION
------------

Install the module using composer.

CONFIGURATION
-------------

Enable Instagram Lite module. Configure and assign
Instagram Lite Block to the desired region.

FEATURES
--------

Instagram Feeds from your own account can be displayed in your website.

BROWSER SUPPORT
---------------

Tested in the latest versions of:

Chrome
Firefox
Safari
iOS

REQUIREMENTS
------------


* Create a Facebook App and configure Instagram test user as mentioned in below link.
[Facebook API setup Token Generation](https://developers.facebook.com/docs/instagram-basic-display-api/getting-started)
* Set the Instagram Basic Display 'Valid OAuth Redirect URIs' field as https://yourdomain.com/instagram-lite/fb-return
* Then use 'Get Instagram Access Token' link in the block configuration page to generate instagram token.
* Token will be automatically renewed one day before the expiry date.

CUSTOMER OWNED INSTAGRAM ACCOUNT
--------------------------------
If the instagram account is owned by the customer.

* Set the Instagram Basic Display 'Valid OAuth Redirect URIs' as https://yourdomain.com/instagram-lite/token
* After adding the Instagram test user ask customer to approve the access here https://www.instagram.com/accounts/manage_access under 'Tester Invites' tab.
* Then share the 'Get Instagram Access Token' Url from block configuration page to the customer.
