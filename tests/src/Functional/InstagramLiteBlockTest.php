<?php

namespace Drupal\Tests\instagram_lite\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Simple test to ensure that main page loads with module enabled.
 *
 * @group instagram_lite
 */
class InstagramLiteBlockTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'instagram_lite',
  ];

  /**
   * The default theme to use with the test.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->placeBlock('instagram_lite');
  }

  /**
   * Tests the instagram_lite block loads properly.
   */
  public function testinstagramLitePage() {
    $account = $this->drupalCreateUser(['access content']);
    $this->drupalLogin($account);

    // Confirm that the front page contains the instagram_lite_block label text.
    $this->assertSession()->pageTextContains('Instagram: No results found.');
  }

}
