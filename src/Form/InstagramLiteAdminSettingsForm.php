<?php

namespace Drupal\instagram_lite\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Instagram Lite settings for this site.
 */
class InstagramLiteAdminSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'instagram_lite_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['instagram_lite.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('instagram_lite.settings');

    $form['instagram_lite_client_id'] = [
      '#default_value' => $config->get('instagram_lite_client_id'),
      '#description' => $this->t('Instagram App ID'),
      '#required' => TRUE,
      '#title' => $this->t('Client ID'),
      '#type' => 'textfield',
    ];
    $form['instagram_lite_client_secret'] = [
      '#default_value' => $config->get('instagram_lite_client_secret'),
      '#description' => $this->t('Instagram App Secret'),
      '#required' => TRUE,
      '#title' => $this->t('Client Secret'),
      '#type' => 'textfield',
    ];
    $form['instagram_lite_developer_email'] = [
      '#default_value' => $config->get('instagram_lite_developer_email'),
      '#description' => $this->t('Customer could send the access token details to this email id.'),
      '#title' => $this->t('Developer Email'),
      '#type' => 'textfield',
    ];
    $form['instagram_lite_app_info'] = [
      '#markup' => 'Check README.MD file to get more information about Facebook App setup.',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('instagram_lite.settings');
    $config
      ->set('instagram_lite_client_id', $form_state->getValue('instagram_lite_client_id'))
      ->set('instagram_lite_client_secret', $form_state->getValue('instagram_lite_client_secret'))
      ->set('instagram_lite_developer_email', $form_state->getValue('instagram_lite_developer_email'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
