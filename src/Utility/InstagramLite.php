<?php

namespace Drupal\instagram_lite\Utility;

use Drupal\Core\Url;

/**
 * Provides utilities only instagram_lite module.
 */
class InstagramLite {

  /**
   * Get logger service.
   */
  public static function getLogger() {
    return \Drupal::service('logger.factory');
  }

  /**
   * Get block manager service.
   */
  public static function getBlockManager() {
    return \Drupal::service('entity_type.manager')->getStorage('block');
  }

  /**
   * Get instagram_lite blocks.
   */
  public static function getBlocks() {
    return \Drupal::entityQuery('block')
      ->condition('plugin', 'instagram_lite')
      ->execute();
  }

  /**
   * Get Instagram app redirect url.
   */
  public static function getRedirectUrl($account_access = 'owner') {
    $route_name = ($account_access != 'owner') ?
      'instagram_lite.token' :
      'instagram_lite.fb_return';
    return Url::fromRoute($route_name, [], [
      'absolute' => TRUE,
      'https' => TRUE,
    ])->toString();
  }

  /**
   * Get Instagram Authentication URL.
   */
  public static function getInstagramAuthUrl($account_access = 'owner') {
    $redirect_uri = self::getRedirectUrl($account_access);

    return Url::fromUri('https://www.instagram.com/oauth/authorize', [
      'query' => [
        'client_id' => \Drupal::config('instagram_lite.settings')->get('instagram_lite_client_id'),
        'redirect_uri' => $redirect_uri,
        'scope' => 'user_profile,user_media',
        'response_type' => 'code',
      ],
      'attributes' => [
        'target' => '_blank',
      ],
      'absolute' => TRUE,
      'response_type' => 'code',
    ]);
  }

}
