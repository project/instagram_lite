<?php

namespace Drupal\instagram_lite\Hook;

/**
 * Class InstagramLiteTheme.
 *
 * Main hook_theme() call.
 */
class InstagramLiteTheme {

  /**
   * Hook hook_theme() main hook call.
   */
  public static function hook($existing, $type, $theme, $path) {
    return [
      'instagram_lite_block' => [
        'variables' => [
          'posts' => NULL,
          'autoplay' => NULL,
          'video' => NULL,
          'post_caption' => NULL,
          'use_blazy' => NULL,
        ],
      ],
    ];
  }

}
