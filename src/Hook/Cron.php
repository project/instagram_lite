<?php

namespace Drupal\instagram_lite\Hook;

use Drupal\instagram_lite\Utility\InstagramLite as InstagramLiteUtility;

/**
 * Class Cron.
 *
 * Main hook_cron() call.
 */
class Cron {

  /**
   * Form hook_cron() main hook call.
   */
  public static function hook() {
    $ids = InstagramLiteUtility::getBlocks();
    $loggerFactory = InstagramLiteUtility::getLogger();
    $blockManager = InstagramLiteUtility::getBlockManager();
    foreach ($ids as $id) {
      $block = $blockManager->load($id);
      $settings = $block->get('settings');
      $renew = $settings['instagram_lite_expires_in'];
      if (time() > $renew) {
        // Renew the token after 1 day.
        $client = \Drupal::service('http_client_factory')->fromOptions(['base_uri' => 'https://graph.instagram.com']);
        $response = $client->request('GET', '/refresh_access_token', [
          'query' => [
            'grant_type' => 'ig_refresh_token',
            'access_token' => $settings['instagram_lite_token'],
          ],
        ]);
        $response = $response->getBody();
        $response = json_decode((string) $response);
        $settings['instagram_lite_token'] = $response->access_token;
        $settings['instagram_lite_expires_in'] = strtotime('+86400 second');
        $block->set('settings', $settings);
        $block->save();
        $loggerFactory->get('instagram_lite')->notice('Token has been refreshed for @block', ['@block' => $id]);
      }
    }
  }

}
