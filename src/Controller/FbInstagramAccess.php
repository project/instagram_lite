<?php

namespace Drupal\instagram_lite\Controller;

use Drupal\Core\Config\ImmutableConfig;
use GuzzleHttp\ClientInterface;
use Drupal\Core\Controller\ControllerBase;
use GuzzleHttp\Exception\RequestException;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\instagram_lite\Utility\InstagramLite as InstagramLiteUtility;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Controller to handle fb response.
 */
class FbInstagramAccess extends ControllerBase {

  /**
   * The http client provider.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The tempstore service.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * Current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * Configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Constructs a new Instagram Lite Block.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The http client provider.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   Private temporary storage factory.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   Request stack service.
   * @param \Drupal\Core\Config\ImmutableConfig $config
   *   The configuration.
   */
  public function __construct(ClientInterface $http_client, PrivateTempStoreFactory $temp_store_factory, RequestStack $requestStack, ImmutableConfig $config) {
    $this->httpClient = $http_client;
    $this->tempStoreFactory = $temp_store_factory->get('instagram_lite');
    $this->request = $requestStack->getCurrentRequest();
    $this->config = $config;
  }

  /**
   * {@inheritdoc}
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The Drupal service container.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('http_client'),
      $container->get('tempstore.private'),
      $container->get('request_stack'),
      $container->get('config.factory')->get('instagram_lite.settings')
    );
  }

  /**
   * Returns redirect based on response.
   */
  public function getToken() {
    $code = $this->request->query->get('code');
    if ($code !== NULL) {
      $short_living_token = $this->getShortLivingToken($code);

      $redirect_back = $this->tempStoreFactory->get('instagram_lite_redirect');
      $this->tempStoreFactory->set('instagram_lite_short_token', $short_living_token);
      $response = new RedirectResponse($redirect_back);
      $response->send();

    }

    return [
      '#markup' => $this->t('Something went wrong!'),
    ];
  }

  /**
   * Get Instagram Access token for client owned account.
   */
  public function getClientToken() {
    $code = $this->request->query->get('code');
    $developer_email = $this->config->get('instagram_lite_developer_email');
    if (isset($code) && ($code != '')) {
      $short_living_token = $this->getShortLivingToken($code, 'customer');

      if ($short_living_token) {
        $response = $this->httpClient->request('GET', 'https://graph.instagram.com/access_token', [
          'query' => [
            'grant_type' => 'ig_exchange_token',
            'client_secret' => $this->config->get('instagram_lite_client_secret'),
            'access_token' => $short_living_token,
          ],
        ]);
        $response = $response->getBody();
        $response = json_decode((string) $response);
        $url = Url::fromUri('mailto:' . $developer_email . '?subject=Instagram%20Lite%20Short%20Living%20Access%20Token&body=Hi%2C%0D%0A%0D%0AHere%20is%20the%20token%20for%20Instagram%20Block%3A%0D%0A%0D%0A' . $response->access_token . '%0D%0A%0D%0AToken%20Expires%3A%0D%0A%0D%0A' . strtotime('+' . $response->expires_in . ' second'));
        $link = Link::fromTextAndUrl($short_living_token, $url);

        $output = 'Access code: ';
        $output .= $link->toString();
        $output .= '<p>Click the access code to share it with the website developer.</p>';
      }
      else {
        $client_redirect_uri = Url::fromRoute('instagram_lite.token', [], [
          'absolute' => TRUE,
          'https' => TRUE,
        ])->toString();
        $client_callback = Url::fromUri('https://www.instagram.com/oauth/authorize', [
          'query' => [
            'client_id' => $this->config->get('instagram_lite_client_id'),
            'redirect_uri' => $client_redirect_uri,
            'scope' => 'user_profile,user_media',
            'response_type' => 'code',
          ],
          ['absolute' => TRUE, 'response_type' => 'code'],
        ]);
        $output = $this->t('@get_instagram_token to generate the Instagram access token.', [
          '@get_instagram_token' => Link::fromTextAndUrl($this->t('Click here'), $client_callback)->toString(),
        ]);
      }
      return [
        '#markup' => $output,
      ];
    }
    else {
      throw new AccessDeniedHttpException();
    }
  }

  /**
   * Get short living instagram access token.
   */
  protected function getShortLivingToken($code, $account_access = 'owner') {
    $redirect_uri = InstagramLiteUtility::getRedirectUrl($account_access);
    try {
      $response = $this->httpClient->request('POST', 'https://api.instagram.com/oauth/access_token', [
        'form_params' => [
          'client_id' => $this->config->get('instagram_lite_client_id'),
          'client_secret' => $this->config->get('instagram_lite_client_secret'),
          'grant_type' => 'authorization_code',
          'redirect_uri' => $redirect_uri,
          'code' => $code,
        ],
      ]);

      $posts = $response->getBody();
      $posts = json_decode((string) $posts);

      return $posts->access_token;
    }
    catch (RequestException $e) {
      watchdog_exception('instagram_lite', $e);
    }
  }

}
