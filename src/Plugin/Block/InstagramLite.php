<?php

namespace Drupal\instagram_lite\Plugin\Block;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\AccountProxyInterface;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use GuzzleHttp\ClientInterface;
use Drupal\Core\Url;
use Drupal\Core\Cache\Cache;
use Drupal\block\BlockForm;
use Drupal\Core\Template\Attribute;
use Drupal\instagram_lite\Utility\InstagramLite as InstagramLiteUtility;

/**
 * Provides a Instragram feed block.
 *
 * @Block(
 *   id = "instagram_lite",
 *   admin_label = @Translation("Instagram Lite Block"),
 * )
 */
class InstagramLite extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The http client provider.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The tempstore service.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Configuration.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new Instagram Lite Block.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The http client provider.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   Private temporary storage factory.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   The configuration.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The logger channel.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ClientInterface $http_client, AccountProxyInterface $currentUser, PrivateTempStoreFactory $temp_store_factory, RequestStack $request_stack, LanguageManagerInterface $language_manager, ConfigFactoryInterface $config, FileSystemInterface $file_system, LoggerChannelInterface $logger, ModuleHandlerInterface $module_handler, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->httpClient = $http_client;
    $this->currentUser = $currentUser;
    $this->tempStoreFactory = $temp_store_factory->get('instagram_lite');
    $this->requestStack = $request_stack;
    $this->languageManager = $language_manager;
    $this->configFactory = $config;
    $this->fileSystem = $file_system;
    $this->logger = $logger;
    $this->moduleHandler = $module_handler;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('http_client'),
      $container->get('current_user'),
      $container->get('tempstore.private'),
      $container->get('request_stack'),
      $container->get('language_manager'),
      $container->get('config.factory'),
      $container->get('file_system'),
      $container->get('logger.factory')->get('instagram_lite'),
      $container->get('module_handler'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'instagram_lite_token' => '',
      'instagram_lite_expires_in' => '',
      'instagram_lite_limit' => 5,
      'instagram_lite_autoplay' => FALSE,
      'instagram_lite_video' => FALSE,
      'instagram_lite_post_caption' => FALSE,
      'instagram_lite_account_owner' => TRUE,
      'instagram_lite_hide_block' => TRUE,
      'instagram_lite_media' => '',
      'instagram_lite_use_blazy' => FALSE,
      'instagram_lite_image_style' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();
    $style_access = FALSE;
    if ($this->moduleHandler->moduleExists('image')) {
      $style_access = TRUE;
    }

    $build = [
      '#attached' => [
        'library' => ['instagram_lite/instagram'],
      ],
      '#theme' => 'instagram_lite_block',
      '#autoplay' => $config['instagram_lite_autoplay'] ?: FALSE,
      '#video' => $config['instagram_lite_video'] ?: FALSE,
      '#post_caption' => $config['instagram_lite_post_caption'] ?: FALSE,
      '#use_blazy' => $config['instagram_lite_use_blazy'] ?: FALSE,
    ];

    try {

      $token = $config['instagram_lite_token'];
      $limit = $config['instagram_lite_limit'] ?: 5;
      $request = $this->httpClient->request('GET', 'https://graph.instagram.com/me/media', [
        'query' => [
          'fields' => 'id,media_type,media_url,thumbnail_url,timestamp,permalink,caption',
          'access_token' => $token,
          'limit' => ($limit > 25) ? $limit : '',
        ],
      ]);

      $posts = $request->getBody()->getContents();
      $posts = Json::decode((string) $posts)['data'];

      $data = [];
      $count = 0;
      if (is_dir($config['instagram_lite_media_path'])) {
        $this->fileSystem->deleteRecursive($config['instagram_lite_media_path']);
        $this->logger->info('Instagram lite media data for %block_id has been purged.', ['%block_id' => $config['instagram_lite_block_id']]);
      }

      $this->fileSystem->mkdir($config['instagram_lite_media_path'], NULL, TRUE);

      foreach ($posts as &$post) {
        $post['attributes'] = new Attribute();
        $post['attributes']['class'] = [];
        $type = $post['media_type'];
        $media_url = $config['instagram_lite_media_path'] . $this->getFileName($post['media_url']);
        if ($count < $limit) {
          if ($type == 'VIDEO' && $config['instagram_lite_video']) {
            continue;
          }
          elseif ($type == 'VIDEO') {
            $post['media_url'] = $post['media_url'];
            $post['video_class'][] = 'insta-video';
            if ($config['instagram_lite_use_blazy']) {
              $post['video_class'][] = 'b-lazy';
            }
          }
          else {
            $image_uri = system_retrieve_file($post['media_url'], $media_url, FALSE, FileSystemInterface::EXISTS_REPLACE);
            $style = $config['instagram_lite_image_style'];
            if (($style_access) && ($style != '')) {
              // Generate image style url.
              $style = $this->entityTypeManager->getStorage('image_style')->load($style);
              $url = $style->buildUrl($image_uri);
            }
            else {
              $url = file_create_url($image_uri);
            }
            $post['attributes']['class'][] = 'insta-image';
            if ($config['instagram_lite_use_blazy']) {
              $post['attributes']['class'][] = 'b-lazy';
              $post['attributes']['data-src'] = $url;
            }
            $post['media_url'] = $url;
          }
          if (isset($post['thumbnail_url'])) {
            $thumbnail_url = $config['instagram_lite_media_path'] . $this->getFileName($post['thumbnail_url']);
            $post['thumbnail_url'] = system_retrieve_file($post['thumbnail_url'], $thumbnail_url, FALSE, FileSystemInterface::EXISTS_REPLACE);
          }
          $count++;
          $data[] = $post;
        }
      }
      if (isset($config['instagram_lite_block_id'])) {
        $block = $this->entityTypeManager->getStorage('block')->load($config['instagram_lite_block_id']);
        $block->getPlugin()->setConfigurationValue('instagram_lite_media', serialize($data));
        $block->save();
      }
      $build['#posts'] = $data;
      if ($this->moduleHandler->moduleExists('blazy') && $config['instagram_lite_use_blazy']) {
        $build['#attached']['library'][] = 'blazy/load';
      }
      return $build;
    }
    catch (RequestException $e) {
      watchdog_exception('instagram_lite', $e);
      $build['#posts'] = unserialize($config['instagram_lite_media']);
      return $build;
    }

  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return Cache::mergeTags(
      parent::getCacheTags(),
      $this->configFactory->get('instagram_lite.settings')->getCacheTags()
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $exp_info = '';
    $client_token_description = '';
    $config = $this->getConfiguration();
    $request = $this->requestStack->getCurrentRequest();
    $token = $config['instagram_lite_token'] ?: $this->t('REPLACE THIS TEXT WITH TOKEN');
    $expires_in = $config['instagram_lite_expires_in'] ?: '';
    $account_owner = ($config['instagram_lite_account_owner'] == TRUE) ? 'owner' : 'customer';

    $request_uri = $request->getRequestUri();
    $request_uri = parse_url($request_uri);

    $query = UrlHelper::filterQueryParameters($this->requestStack->getCurrentRequest()->query->all(), ['_wrapper_format']);

    $instagram_lite_redirect = Url::fromUserInput($request_uri['path'], [
      'query' => [
        $query,
      ],
    ])->toString();
    $this->tempStoreFactory->set('instagram_lite_redirect', $instagram_lite_redirect);

    $callback = InstagramLiteUtility::getInstagramAuthUrl();
    $client_callback = InstagramLiteUtility::getInstagramAuthUrl('customer');

    $short_living_token = $this->tempStoreFactory->get('instagram_lite_short_token');
    // Get long lived access token.
    if (isset($short_living_token)) {
      $response = $this->httpClient->request('GET', 'https://graph.instagram.com/access_token', [
        'query' => [
          'grant_type' => 'ig_exchange_token',
          'client_secret' => $this->configFactory->get('instagram_lite.settings')->get('instagram_lite_client_secret'),
          'access_token' => $short_living_token,
        ],
      ]);
      $response = $response->getBody();
      $response = json_decode((string) $response);
      $token = $response->access_token;
      $expires_in = strtotime('+86400 second');
      $this->tempStoreFactory->delete('instagram_lite_short_token');
    }
    $form['instagram_lite_image_style'] = [
      '#type' => 'select',
      '#title' => $this->t('Image style'),
      '#options' => image_style_options(),
      '#default_value' => $config['instagram_lite_image_style'] ?: '',
    ];
    $form['instagram_lite_account_owner'] = [
      '#type' => 'radios',
      '#title' => $this->t('Instagram account ownership'),
      '#options' => [
        'owner' => $this->t('Own account'),
        'customer' => $this->t('Customer account'),
      ],
      '#required' => TRUE,
      '#default_value' => $account_owner,
    ];

    $form['instagram_lite_token'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Instagram Token'),
      '#rows' => 4,
      '#cols' => 5,
      '#required' => TRUE,
      '#default_value' => $token,
      '#states' => [
        'visible' => [
          ':input[name="settings[instagram_lite_account_owner]"]' => ['value' => 'owner'],
        ],
      ],
    ];

    if ($this->configFactory->get('instagram_lite.settings')->get('instagram_lite_client_id') == NULL) {
      $token_description = $this->t('@configure Instagram Client ID and Secret to generate access token.', [
        '@configure' => Link::fromTextAndUrl($this->t('Configure'), Url::fromRoute('instagram_lite.admin_settings_form'))->toString(),
      ]);
    }
    else {
      $token_description = $this->t('Click here to generate @get_instagram_token.', [
        '@get_instagram_token' => Link::fromTextAndUrl($this->t('Get Instagram Access Token'), $callback)->toString(),
      ]);
      $client_token_description = $this->t('Copy and give this link url @get_instagram_token to customer to get the access token.', [
        '@get_instagram_token' => Link::fromTextAndUrl($this->t('Get Instagram Access Token'), $client_callback)->toString(),
      ]);
      if ((($token != '') && isset($response)) || $expires_in != '') {
        $exp_info = ' ' . $this->t('Token renew on @expires_in', [
          '@expires_in' => date("m/d/Y h:i:s a", $expires_in),
        ]);
        $token_description .= $exp_info;
      }
    }

    $form['instagram_lite_token']['#description'] = $token_description;

    $form['instagram_lite_token_customer'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Instagram Token'),
      '#rows' => 4,
      '#cols' => 5,
      '#required' => TRUE,
      '#default_value' => $token,
      '#description' => ($client_token_description != '') ? $client_token_description : $token_description . $exp_info,
      '#states' => [
        'visible' => [
          ':input[name="settings[instagram_lite_account_owner]"]' => ['value' => 'customer'],
        ],
      ],
    ];

    $form['instagram_lite_limit'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Instagram Feed Limit'),
      '#required' => TRUE,
      '#default_value' => $config['instagram_lite_limit'] ?: '',
    ];
    $form['instagram_lite_video'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide videos'),
      '#required' => FALSE,
      '#default_value' => $config['instagram_lite_video'] ?: '',
    ];
    $form['instagram_lite_autoplay'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Autoplay Instagram Videos'),
      '#required' => FALSE,
      '#default_value' => $config['instagram_lite_autoplay'] ?: '',
      '#states' => [
        'visible' => [
          ':input[name="settings[instagram_lite_video]"]' => ['checked' => FALSE],
        ],
      ],
    ];
    $form['instagram_lite_post_caption'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display Instagram post caption'),
      '#required' => FALSE,
      '#default_value' => $config['instagram_lite_post_caption'] ?: '',
    ];
    $form['instagram_lite_hide_block'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide block if empty'),
      '#required' => FALSE,
      '#default_value' => $config['instagram_lite_hide_block'] ?: '',
    ];
    if ($this->moduleHandler->moduleExists('blazy')) {
      $form['instagram_lite_use_blazy'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Use Blazy'),
        '#required' => FALSE,
        '#default_value' => $config['instagram_lite_use_blazy'],
      ];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {
    $instagram_lite_token = $form_state->getValue('instagram_lite_token');
    $account = $form_state->getValue('instagram_lite_account_owner') == 'owner';
    $instagram_lite_token_customer = $form_state->getValue('instagram_lite_token_customer');
    if (($instagram_lite_token == $instagram_lite_token_customer) && ($instagram_lite_token_customer == $this->t('REPLACE THIS TEXT WITH TOKEN'))) {
      if ($account) {
        $form_state->setErrorByName('instagram_lite_token', $this->t('Please enter token.'));
      }
      else {
        $form_state->setErrorByName('instagram_lite_token_customer', $this->t('Please enter token.'));
      }
    }
  }

  /**
   * Get file name from URL.
   */
  protected function getFileName($url) {
    $info = pathinfo(parse_url($url)['path']);
    return $info['basename'];
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\TempStore\TempStoreException
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $account = $form_state->getValue('instagram_lite_account_owner') == 'owner';
    $this->configuration['instagram_lite_token'] = $account ? $form_state->getValue('instagram_lite_token') :
      $form_state->getValue('instagram_lite_token_customer');
    $this->configuration['instagram_lite_limit'] = $form_state->getValue('instagram_lite_limit');
    $this->configuration['instagram_lite_video'] = $form_state->getValue('instagram_lite_video');
    $this->configuration['instagram_lite_autoplay'] = $form_state->getValue('instagram_lite_autoplay');
    $this->configuration['instagram_lite_expires_in'] = strtotime('+86400 second');
    $this->configuration['instagram_lite_post_caption'] = $form_state->getValue('instagram_lite_post_caption');
    $this->configuration['instagram_lite_account_owner'] = $account;
    $this->configuration['instagram_lite_hide_block'] = $form_state->getValue('instagram_lite_hide_block');
    $this->configuration['instagram_lite_use_blazy'] = $form_state->getValue('instagram_lite_use_blazy');
    $this->configuration['instagram_lite_image_style'] = $form_state->getValue('instagram_lite_image_style');
    $buildInfo = $form_state->getBuildInfo();
    $instagram_lite_path = 'public://instagram_lite/';
    if ($buildInfo['callback_object'] instanceof BlockForm) {
      $blockId = $buildInfo['callback_object']->getEntity()->id();
      $instagram_lite_path .= $blockId . '/';
      $this->configuration['instagram_lite_block_id'] = $blockId;
    }
    $this->configuration['instagram_lite_media_path'] = $instagram_lite_path;
  }

}
